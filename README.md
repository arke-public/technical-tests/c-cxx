# Test technique

Ce test technique a pour but d'évaluer vos connaissances en C, seule la **partie 1** est **obligatoire**, la **partie 2** en version C++ est un plus.

Ce qui sera évalué :
- le niveau de connaissance du langage
- la structuration objet
- l'organisation du projet
- la lisibilité

## Données

Dans les différentes questions, nous allons manipuler un objet (structure en C) nommé *Peripheral*. Il sera composé de :
 - Un nom (30 caractères maximum)
 - Un type (*VIRTUAL* ou *PHYSICAL*)
 - De 0 à 6 entrées/sorties (nommées IO)
 
Cet objet (structure) possèdera au minimum 5 fonctions (méthodes en C++) :

 - une fonction permettant d'initialiser un Peripheral avec son nom, son type et ses entrées / sorties (constructeur en C++)
 - une fonction permettant d'accéder au nom du Peripheral
 - une fonction permettant d'accéder au type du Peripheral
 - une fonction permettant de connaitre le nombre d'entrées / sorties
 - une fonction permettant d'accéder à la n iéme entrée / sortie
 
Une entrée / sortie (*IO*) sera composée de :
- Un identifiant numérique
- Une valeur booléenne
 
## Partie 1 - Création d'une liste chainée C

### Exercice A - Implémentation statique

Une grande partie de votre travail consistera à développer sur des micro-contrôleurs. Certains d'entre eux ne possèdent
 pas de pile (_heap_) et ne peuvent donc pas utiliser d'allocation dynamique (*malloc* / *free*).

Implémenter une liste de Peripheral **entièrement statique** permettant au minimum :
- l'ajout d'un Peripheral
- l'accès à un Peripheral

Créer un programme d'exemple **test-technique-c-static-list** permettant de :
1. créer une liste de 5 Peripheral avec les IO initialisés à *false* :
    - A avec 1 IO
    - B avec 2 IOs
    - C sans IO
    - D avec 1 IO
    - E avec 3 IOs
2. Parcourir les IOs de la liste en activant (passage à *true*) une IO sur deux.
3. Ajouter un nouveau Peripheral F avec une unique IO initialisée à *true*
4. Parcourir l'ensemble des Peripheral en affichant l'état des IOs

### Exercice B - Implémentation dynamique

La seconde partie de votre travail sera de développer des interfaces graphiques (mobile et écran) sur des systèmes plus evolués.
Reprendre l'énoncé de l'exercice 1 mais en utilisant une liste contenant des objets alloués dynamiquement.

## Partie 2 - Création d'une liste chainée C++ (Optionnel)

Les interfaces graphiques que nous réalisons sont faites en C++ 17 et en Qt. Le C++ serait donc un plus : pour évaluer
 votre niveau, refaire la partie 1 en C++ en utilisant uniquement la STL.

# Remarque

Même si votre code ne fonctionne pas, envoyez-le, car le cheminement est aussi important que le résultat.